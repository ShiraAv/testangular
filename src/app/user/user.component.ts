import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import {User} from './user';

@Component({
  selector: 'jce-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs:['user']
})
export class UserComponent implements OnInit {
  user:User;//create varibale variable user that is class user 
  @Output() deleteEvent = new EventEmitter<User>();
  @Output() editvent = new EventEmitter<User>();
  isEdit : boolean = false;//start with is edit button is false (not editing)
  editButtonText = 'Edit';//we will se Edit on button
  constructor() { }
  sendDelete(){
    this.deleteEvent.emit(this.user);
  }
  toggleEdit(){
     //update parent about the change
     this.isEdit = !this.isEdit; //for toggle property, isEdit can be true or false and not(!) will be other way
     this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit';    
     if(this.isEdit){
       this.editvent.emit(this.user);
     }

  }
  ngOnInit() {
  }

}
