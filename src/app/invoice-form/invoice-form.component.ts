import { Component, OnInit, Output, EventEmitter  } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Invoice } from '../invoice/invoice';
import { InvoicesService } from '../invoices/invoices.service';



@Component({
  selector: 'jce-invoice-form',
  templateUrl: './invoice-form.component.html',
  styleUrls: ['./invoice-form.component.css']
})
export class InvoiceFormComponent implements OnInit {
  @Output() invoiceAddedEvent = new EventEmitter<Invoice>();

   invoice:Invoice = {name:'', amount:''};

  onSubmit(form:NgForm){
  console.log(form);
  this._invoicesService.addInvoice(this.invoice);
  console.log(this.invoice);
  this.invoice= {
    name:'',
    amount:''
    };
}

  constructor(private _invoicesService:InvoicesService) { }

  ngOnInit() {
    this._invoicesService.getInvoices().subscribe(invoicesData => {this.invoice=invoicesData});


  }

}


